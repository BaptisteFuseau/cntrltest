package Exercice2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDateTime;

public class ReservationTest {
    private Reservation reservation;

    @BeforeEach
    public void setUp() {
        reservation = new Reservation();
    }

    @Test
    public void testReservationValide() {
        int idPatient = 1;
        int idMedecin = 1;
        LocalDateTime dateHeureRdv = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean resultatAttendu = true;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        Assertions.assertTrue(resultatObtenu);
    }

    @Test
    public void testReservationPatientInvalide() {
        int idPatient = -1; //Identifiant invalide
        int idMedecin = 1;
        LocalDateTime dateHeureRdv = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean resultatAttendu = false;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        Assertions.assertFalse(resultatObtenu);
    }

    @Test
    public void testReservationMedecinInvalide() {
        int idPatient = 1;
        int idMedecin = -1; //Identifiant invalide
        LocalDateTime dateHeureRdv = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        boolean resultatAttendu = false;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        Assertions.assertFalse(resultatObtenu);
    }

    @Test
    public void testReservationDateIndisponible() {
        int idPatient = 1;
        int idMedecin = 1;
        LocalDateTime dateHeureRdv = LocalDateTime.of(2022, 4, 1, 10, 0, 0); //Déjà réservées

        //On effectue une réservation pour bloquer la date
        reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        boolean resultatAttendu = false;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        Assertions.assertFalse(resultatObtenu);
    }

    @Test
    public void testReservationPatientOccupe() {
        int idPatient = 1;
        int idMedecin = 1;
        LocalDateTime dateHeureRdv = LocalDateTime.of(2022, 4, 1, 10, 0, 0);

        //On effectue une réservation pour bloquer la date
        reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        boolean resultatAttendu = false;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        Assertions.assertFalse(resultatObtenu);
    }

    @Test
    public void testReservationMedecinOccupe() {
        int idPatient = 1;
        int idMedecin = 1;
        LocalDateTime dateHeureRdv1 = LocalDateTime.of(2022, 4, 1, 10, 0, 0);
        LocalDateTime dateHeureRdv2 = LocalDateTime.of(2022, 4, 1, 11, 0, 0);
        LocalDateTime dateHeureRdv3 = LocalDateTime.of(2022, 4, 1, 12, 0, 0);
        LocalDateTime dateHeureRdv4 = LocalDateTime.of(2022, 4, 1, 13, 0, 0);

        //On effectue des réservations pour bloquer les dates
        reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv1);
        reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv2);
        reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv3);
        reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv4);

        LocalDateTime dateHeureRdv5 = LocalDateTime.of(2022, 4, 1, 14, 0, 0); //Médecin déjà occupé

        boolean resultatAttendu = false;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv5);

        Assertions.assertFalse(resultatObtenu);
    }
    
    @ParameterizedTest(name = "Réservation avec Params - Patient: {0}, Médecin: {1}, Date et heure: {2}")
    @CsvSource({
            "1, 1, 2022-04-01T10:00:00",
            "2, 1, 2022-04-01T11:00:00",
            "1, 2, 2022-04-02T13:00:00",
            "2, 2, 2022-04-02T14:00:00",
            "3, 2, 2022-04-02T15:00:00",
            "4, 2, 2022-04-02T16:00:00"
    })
    public void testReservationParametre(int idPatient, int idMedecin, LocalDateTime dateHeureRdv) {
        Reservation reservation = new Reservation();

        boolean resultatAttendu = true;
        boolean resultatObtenu = reservation.reserverRendezVous(idPatient, idMedecin, dateHeureRdv);

        Assertions.assertEquals(resultatAttendu, resultatObtenu);
    }

}
