package Exercice1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PaymentIntegrationTest {
	
	@Test
	//on vérifie que le paiement est valide (vérification de la carte, de la transaction et du paiement)
	public void testPaiementValide() {
		PaymentProcessor paymentProcessor = new PaymentProcessor();
        CarteCreditManager carteCreditManager = new CarteCreditManager();
        PaymentGateway paymentGateway = new PaymentGateway();
        
        double montant = 20.0;
        String numCarte = "1234567891234567";
        String cvv = "123";
        
        //Test de la carte valide 
        boolean cardValid = carteCreditManager.verifyCard(numCarte, cvv);
        Assertions.assertTrue(cardValid);
        
        //on récupère le numéro de transaction
        String idTransaction = paymentGateway.makePayment(montant);
        //on vérifie qu'il n'est pas null
        Assertions.assertNotNull(idTransaction);
        
        //on vérifie que la procédure de paiement passe
        boolean paymentProcessed = paymentProcessor.processPayment(montant, numCarte, cvv);
        Assertions.assertTrue(paymentProcessed);
	}
	
	@Test
	//on regarde si on essaye avec une carte invalide
	public void testCarteInvalide() {
		PaymentProcessor paymentProcessor = new PaymentProcessor();
        CarteCreditManager carteCreditManager = new CarteCreditManager();
        PaymentGateway paymentGateway = new PaymentGateway();
        
        double montant = 20.0;
        String numCarte = "123456789";
        String cvv = "123";
        
        //Test de la carte valide 
        boolean cardValid = carteCreditManager.verifyCard(numCarte, cvv);
        Assertions.assertFalse(cardValid);
	}
	
	@Test
	//on vérifie que cela nous remonte bien quand le numéro de transaction est invalide
	public void testTransactionInvalide() {
		PaymentProcessor paymentProcessor = new PaymentProcessor();
        CarteCreditManager carteCreditManager = new CarteCreditManager();
        PaymentGateway paymentGateway = new PaymentGateway();
        
        double montant = -12.12;
        String numCarte = "1234567891234567";
        String cvv = "123";
        
        //Test de la carte valide 
        boolean cardValid = carteCreditManager.verifyCard(numCarte, cvv);
        Assertions.assertTrue(cardValid);
        
        //on récupère le numéro de transaction
        String idTransaction = paymentGateway.makePayment(montant);
        //on vérifie qu'il est null (échec)
       	System.out.println(idTransaction);
        Assertions.assertNull(idTransaction);
	}
}
