package Exercice1;

public class CarteCreditManager {
	
	//Vérification des infos de la cartes de crédit
	public boolean verifyCard(String cardNumber, String cvv) {
        //On return true si le numéro de carte est égal à 16 chiffres et le cvv est égal à 3
        return cardNumber.length() == 16 && cvv.length() == 3;
	}
}
