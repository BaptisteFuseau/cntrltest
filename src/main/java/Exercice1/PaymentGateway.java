package Exercice1;

public class PaymentGateway {
	
	public String makePayment(double amount) {
        //traitement avec le service de paiement tiers qui renvoit un id de transaction
		//si on reçois -12,12, on return null afin de tester.
		if (amount == -12.12) 
			return null;
		
		//sinon on renvoie l'id
        return "id_transaction";
	}
}
