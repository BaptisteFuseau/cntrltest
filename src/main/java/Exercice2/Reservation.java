package Exercice2; 

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

//Class de réservation des rendez-vous
public class Reservation {
    private Map<LocalDateTime, Integer> medecinRendezVous;
    private Map<LocalDateTime, Integer> patientRendezVous;

    public Reservation() {
        medecinRendezVous = new HashMap<LocalDateTime, Integer>();
        patientRendezVous = new HashMap<LocalDateTime, Integer>();
    }

    //Méthode de réservation d'un rendez-vous
    public boolean reserverRendezVous(int idPatient, int idMedecin, LocalDateTime dateHeureRdv) {
        if (!estPatientValide(idPatient)) {
            return false;
        }

        if (!estMedecinValide(idMedecin)) {
            return false;
        }

        if (!estMedecinDisponible(idMedecin, dateHeureRdv)) {
            return false;
        }

        if (patientOccupe(idPatient, dateHeureRdv)) {
            return false;
        }

        if (medecinOccupe(idMedecin, dateHeureRdv)) {
            return false;
        }

        //Effectuer la réservation du rendez-vous
        medecinRendezVous.put(dateHeureRdv, idMedecin);
        patientRendezVous.put(dateHeureRdv, idPatient);

        return true;
    }

    private boolean estPatientValide(int idPatient) {
        //Vérifier si le patient existe dans la base de données
    	//Si < à 0, il n'existe pas 
    	if (idPatient > 0)
    		return true; 
    	return false;
    }

    private boolean estMedecinValide(int idMedecin) {
        //Vérifie si le médecin existe dans la base de données
    	//Si < à 0, il n'existe pas 
    	if (idMedecin > 0)
    		return true; 
    	return false;
    }

    private boolean estMedecinDisponible(int idMedecin, LocalDateTime dateHeureRdv) {
        //Vérifie si le médecin est disponible à la date - heure spécifiées
        return true;
    }

    private boolean patientOccupe(int idPatient, LocalDateTime dateHeureRdv) {
        //Vérifie si le patient a déjà un rendez-vous à la même date - heure
        for (Map.Entry<LocalDateTime, Integer> entry : patientRendezVous.entrySet()) {
            if (entry.getValue() == idPatient && entry.getKey().isEqual(dateHeureRdv)) {
                return true;
            }
        }
        return false;
    }

    private boolean medecinOccupe(int idMedecin, LocalDateTime dateHeureRdv) {
        //Vérifie si le médecin a déjà atteint le maximum de rendez-vous pour cette date
        int rendezVousCount = 0;
        for (Map.Entry<LocalDateTime, Integer> entry : medecinRendezVous.entrySet()) {
            if (entry.getValue() == idMedecin && entry.getKey().toLocalDate().equals(dateHeureRdv.toLocalDate())) {
                rendezVousCount++;
            }
        }
        return rendezVousCount >= 4;
    }
}
